<?php

/*
<entry kind="dir">
  <name>tags</name>
  <commit revision="1">
    <author>halkeye</author>
    <date>2005-10-31T00:54:52.056186Z</date>
  </commit>
</entry>

<entry kind="dir">
  <name>trunk</name>
  <commit revision="66">
    <author>halkeye</author>
    <date>2006-07-09T21:17:31.582522Z</date>
  </commit>
</entry>
*/
class subversion_ls_parser
{
  /**
  var $current_item;
  var $listing; // Listing data
  */
  function parse ($XMLData)
  {
    $parser = xml_parser_create();
    $this->listing = array();
    $this->current_item = null;

    //This is the RIGHT WAY to set everything inside the object.
    xml_set_object ( $parser, $this );

    xml_set_element_handler ( $parser, 'tag_start', 'tag_end' );
    xml_set_character_data_handler ( $parser, 'tag_content' );

    xml_parse ( $parser, $XMLData );

  }

/*
<entry kind="dir">
  <name>branches</name>
  <commit revision="1">
    <author>halkeye</author>
    <date>2005-10-31T00:54:52.056186Z</date>
  </commit>
</entry>
*/
  function tag_start($parser,$tag, $attr) {
    global $logEntry;
    switch (strtolower($tag)) {
      case 'entry':
        $this->current_item = new StdClass;
        $this->current_item->type = strtolower($attr['KIND']);
        break;
      case 'commit':
        $this->current_item->revision = strtolower($attr['REVISION']);
        break;
    }
  }

  function tag_end($parser,$tag) {
    if (!$this->current_item) {
      return;
    }

    $tag = strtolower($tag); 

    switch ($tag) {
      case 'entry':
        $this->listing[] = $this->current_item;
        $this->current_item = null;
        break;
      case 'commit':
        break;
      case 'date':
        $time = $logEntry['data'];
        list($time) = explode('.', $time);
        $t = strtotime($time);
        $zone = intval(date("O"))/100;
        $t += $zone*60*60;
        $this->current_item->date = $t;
        break;
      default:
        $this->current_item->$tag = $this->data;
        break;
    }
    $this->data = '';
  }

  function tag_content($parser,$data) {
    if (!$this->current_item) {
      return;
    }
    $this->data .= $data;
  }

  /* ... */

}

?>

