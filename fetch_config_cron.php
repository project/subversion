#!/usr/bin/php
<?php

$config = new StdClass;

$config->login = "";
$config->pass  = "";
$config->base_url = "http://www.kodekoan.com/index.php?q=";
$config->repository = 1;
$config->output_file['authz'] = '/home/halkeye/vhosts/kodekoan.com/svn/kodegame/conf/authz';
$config->output_file['passwd'] = '/home/halkeye/vhosts/kodekoan.com/svn/kodegame/conf/passwd';

@include_once("fetch_config_cron_config.php");


$cookiejar = tempnam('/tmp', 'cookiejar');

$ch = curl_init();
$url =  $config->base_url . "user";
// set URL and other appropriate options
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_COOKIEJAR, $cookiejar);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
curl_setopt($ch, CURLOPT_POSTFIELDS, "edit%5Bname%5D={$config->login}&edit%5Bpass%5D={$config->pass}&op=Log+in&edit%5Bform_id%5D=user_login");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);  // return into a variable
curl_setopt($ch, CURLOPT_VERBOSE, 0);  // return into a variable
curl_setopt($ch, CURLOPT_HEADER, 1);

$content = curl_exec($ch);         // run the whole process
#preg_match_all('|Set-Cookie: (.*);|U', $content, $results);   
#$cookies = implode(';', $results[1]);
#curl_setopt($ch, CURLOPT_COOKIE,  $cookies);

foreach (array('authz', 'passwd') as $type) {
    $url =  $config->base_url . "admin/subversion/repositories/export-{$type}/{$config->repository}";
    // set URL and other appropriate options
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);  // return into a variable
    $result = curl_exec($ch);         // run the whole process

    if ($result) {
        $fp = fopen($config->output_file[$type], "w");
        fwrite($fp, $result);
        fclose($fp);
    }
    else {
        print "Unable to fetch: $url\n";
    }
}

// close curl resource, and free up system resources
curl_close($ch);
unlink( $cookiejar);

?>
